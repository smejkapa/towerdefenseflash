# Tower defense flash

A simple tower defense game made in Flash (Sterling framework), as one of three games submitted as a semestral project 
to [Game Development Middleware](https://gamedev.cuni.cz/study/courses-history/courses-2016-2017/game-development-middleware-winter-201617/) at Faculty of Mathematics and Physics, Charles University.
