package td.mechanics 
{
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import td.utils.pools.ObjectPool;
	import td.Context;
	/**
	 * ...
	 * @author Aldarion
	 */
	public class Bullet extends Sprite
	{
		public static var POOL: ObjectPool = new ObjectPool("Bullet", "Visuals", getNew);
		
		private var visual: Quad;
		
		public function Bullet() 
		{
			visual = new Quad();
			visual.x = 0; visual.y = 0;
			
			visual.width = 5;
			visual.height = 5;
			visual.color = 256 * 256 * 256;
			
			addEventListener(Event.ADDED_TO_STAGE, odAddedToStage);
		}
		
		private function onAddedToStage(e: * = null) : void {
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			addChild(visual);
		}
		
		public function reset() : void {
			removeChild(visual);
		}
		
		public function shoot(x: Number, y: Number, rotation: Number, speed: Number)
		{
			this.x = x;
			this.y = y;
			
			Context.screenManager.get().addChild(this);
		}
		
		public static function getNew() : Bullet{
			return new Bullet();
		}
	}

}