package td.mechanics 
{
	import flash.geom.Point;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import td.screens.LevelScreen;
	import td.utils.MathUtils;
	import td.Context;
	/**
	 * ...
	 * @author Aldarion
	 */
	public class Enemy extends Sprite
	{
		private var speed:int;
		private var reward:int;
		private var hp:int;
		private var image:Image;
		private var lvl:LevelScreen
		private var path:Array;
		private var currentNodeIdx:int = 0;
		public function isEscaped():Boolean { return currentNodeIdx >= path.length; }
		public function isDead():Boolean { return hp <= 0; }
		private var forward:Point = new Point(1, 0);
		
		public function Enemy(speed:Number, reward:int, hp:int, img:Image, lvl:LevelScreen, path:Array) 
		{
			this.speed = Math.floor(speed * Context.values.tileUnit);
			this.reward = reward;
			this.hp = hp;
			this.image = img;
			addChild(image);
			this.lvl = lvl;
			this.path = path;
			alignPivot();
			this.x = path[0].x;
			this.y = path[0].y;
			this.width = Context.values.tileUnit;
			this.height = Context.values.tileUnit;
			addEventListener(EnterFrameEvent.ENTER_FRAME, onFrame);
		}
		
		private function setRotation(a:int, b:int):void
		{
			var next:Point = path[b].subtract(path[a]);
			next.normalize(1);
			this.rotation = Math.acos(MathUtils.dot(next, forward));
			if (next.y < 0)
			{
				this.rotation = -this.rotation;
			}
		}
		
		public function takeDmg(dmg:int):void
		{
			hp -= dmg;
			if (isDead())
			{
				remove();
				var actualReward:int = (reward + 1) * Math.random();
				
				for (var i:int = 0; i < actualReward; i++)
				{
					var cX:int = Context.values.tileUnit * Math.random() - Context.values.tileUnit / 2;
					var cY:int = Context.values.tileUnit * Math.random() - Context.values.tileUnit / 2;
					var coin:Coin = new Coin(x + cX, y + cY, lvl);
					lvl.addChild(coin);
				}
			}
		}
		
		private function remove():void
		{
			removeEventListeners();
		}
		
		private function onFrame(event:EnterFrameEvent):void
		{
			var currentNode:Point = path[currentNodeIdx];
			var dist:Number = MathUtils.distance(currentNode.x, currentNode.y, x, y);
			if (dist < 5)
			{
				currentNodeIdx++;
				if (isEscaped())
				{
					lvl.enemyEscaped(this);
					remove();
					return;
				}
				else // Go to next node
				{
					currentNode = path[currentNodeIdx];
					setRotation(currentNodeIdx - 1, currentNodeIdx);
				}
			}
			
			// Move towards next node
			var direction:Point = currentNode.subtract(new Point(x, y));
			direction.normalize(1);
			x += direction.x * speed * event.passedTime;
			y += direction.y * speed * event.passedTime;
		}
	}

}