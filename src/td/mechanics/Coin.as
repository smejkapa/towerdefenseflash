package td.mechanics 
{
	import starling.display.Image;
	import starling.display.Sprite;
	import td.screens.LevelScreen;
	import td.Context;
	import starling.events.TouchEvent;
	import starling.events.Touch;
	import starling.events.TouchPhase;
	/**
	 * ...
	 * @author Aldarion
	 */
	public class Coin extends Sprite
	{
		private var lvl:LevelScreen;
		private var image:Image;
		
		public function Coin(x:int, y:int, lvl:LevelScreen) 
		{
			this.x = x;
			this.y = y;
			
			image = Context.newImage("coin");
			image.width = Context.values.tileUnit / 4;
			image.height = image.width;
			addChild(image);
			
			this.width = image.width;
			this.height = width;
			alignPivot();
			
			this.lvl = lvl;
			
			addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		private function onTouch(event: TouchEvent):void
		{
			var touch:Touch = event.getTouch(this.stage);
			if (touch)
			{
				if (touch.phase == TouchPhase.BEGAN)
				{
					lvl.coinPickedUp(1);
					removeEventListeners();
					lvl.removeChild(this);
				}
			}
		}
	}

}