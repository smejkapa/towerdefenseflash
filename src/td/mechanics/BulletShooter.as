package td.mechanics 
{
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import td.utils.Utils;
	/**
	 * ...
	 * @author Aldarion
	 */
	public class BulletShooter extends Sprite
	{
		private var timeNextShoot: Number = 0.5;
		private var nextShoot: Number = 0;
		private var lastTime: Number = 0;
		
		public function BulletShooter() 
		{
			
		}

		private function onAddedToStage(e: * = null) : void {
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			addEventListener(EnterFrameEvent.ENTER_FRAME, tick);
		}
		
		private function tick(e: * = null) : void{
			if (lastTime == 0){
				lastTime = Utils.time();
				return;
			}
			var delta: Number = Utils.time() - lastTime;
			lastTime = Utils.time();
			
			nextShoot -= delta;
			
			if (nextShoot <= 0)
			{
				shoot();
				nextShoot = timeNextShoot;
			}
		}
		
		private function shoot() : void{
			var bullet : Bullet = Bullet.POOL.get();
			
			bullet.shoot();
		}
		
		public function reset() : void{
			try{
				removeEventListener(Event.ENTER_FRAME, tick)
			} catch (e: Error) {
				
			}
			
			timeNextShoot = 0.5;
			nextShoot = 0;
		}
	}

}