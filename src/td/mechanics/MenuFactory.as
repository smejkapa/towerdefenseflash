package td.mechanics 
{
	import starling.display.Sprite;
	import starling.display.Image;
	import td.Context;
	import td.utils.draw.Circle;
	
	/**
	 * ...
	 * @author Aldarion
	 */
	public class MenuFactory 
	{
		
		public static function makeButton(code:String, x:int, y:int):Tile
		{
			switch (code) 
			{
				case "T1":
					return new Tile(code, x * Context.values.tileUnit, y * Context.values.tileUnit, Context.newImage("towerDefense_tile249.png"), null);
				case "T2":
					return new Tile(code, x * Context.values.tileUnit, y * Context.values.tileUnit, Context.newImage("towerDefense_tile250.png"), null);
				case "Dbg":
					return new Tile(code, x * Context.values.tileUnit, y * Context.values.tileUnit, Context.newImage("towerDefense_tile250.png"), null);
				default:
					throw "Unknown menu code";
			}
		}
		
		public static function makeTowerImage(code:String):Sprite
		{
			var img:Image;
			var sprite:Sprite = new Sprite();
			var circle:Circle = new Circle();
			var radius:int = Context.values.tileUnit;
			switch (code) 
			{
				case "T1":
					img = Context.newImage("towerDefense_tile249.png");
					radius = TowerRocket.range;
					break;
				case "T2":
					img = Context.newImage("towerDefense_tile250.png");
					radius = TowerGun.range;
					break;
				default:
					return null;
			}
			circle.drawCircle(radius);
			img.width = Context.values.tileUnit;
			img.height = Context.values.tileUnit;
			img.alignPivot();
			sprite.addChild(circle);
			sprite.addChild(img);
			sprite.width = radius * 2;
			sprite.height = radius * 2;
			return sprite;
		}
	}

}