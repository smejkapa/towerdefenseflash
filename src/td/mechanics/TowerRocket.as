package td.mechanics 
{
	import starling.display.Image;
	import starling.events.EnterFrameEvent;
	import td.screens.LevelScreen;
	import td.Context;
	import flash.geom.Point;
	import com.greensock.TweenLite;
	import com.greensock.easing.RoughEase;
	import com.greensock.easing.Linear;
	import com.greensock.easing.Expo;
	import flash.media.SoundTransform;
	
	/**
	 * ...
	 * @author Aldarion
	 */
	public class TowerRocket extends Tower
	{
		public static var range:int = 4 * Context.values.tileUnit;
		public static const damage:int = 10;
		
		private var recoilVec:Point;
		
		public function TowerRocket(x:int, y:int, img:Image, lvl:LevelScreen) 
		{
			super("T1", x, y, img, lvl, range, TowerRocket.damage, 4000);
			shootPoint  = new Point(Context.values.tileUnit / 2, 0);
			addEventListener(EnterFrameEvent.ENTER_FRAME, onFrame);
			shootPoint = new Point(Context.values.tileUnit / 2, Context.values.tileUnit * 0.078);
		}
		
		override public function clicked():void
		{
			super.clicked();
		}
		
		private function onFrame(e: * = null):void
		{
		}
		
		protected override function onShoot():void
		{
			Context.assets.manager.playSound("rpg-launcher-three-shots", 7700, 0, new SoundTransform(0.5));
			recoilVec = new Point(target.x - x, target.y - y);
			recoilVec.normalize(3);
			TweenLite.to(this, 0.5, {x:this.x - recoilVec.x, y:this.y - recoilVec.y, ease:Expo.easeOut, onComplete:reverseRecoil});
			
			super.onShoot();
		}
		
		private function reverseRecoil():void
		{
			TweenLite.to(this, 0.5, {x:this.x + recoilVec.x, y:this.y + recoilVec.y, ease:Expo.easeOut});
		}
	}

}