package td.mechanics 
{
	import starling.display.Image;
	import starling.events.EnterFrameEvent;
	import td.screens.LevelScreen;
	import td.Context;
	import flash.geom.Point;
	import com.greensock.TweenLite;
	import com.greensock.easing.RoughEase;
	import com.greensock.easing.Linear;
	import flash.media.SoundTransform;
	
	/**
	 * ...
	 * @author Aldarion
	 */
	public class TowerGun extends Tower
	{
		public static var range:int = 2 * Context.values.tileUnit;
		public static const damage:int = 1;
		private static var shootPoints:Array = [
			new Point(Context.values.tileUnit * 0.398, Context.values.tileUnit * 0.062),
			new Point(Context.values.tileUnit * 0.585, Context.values.tileUnit * 0.062)];
		private var lastSpUsed:int = 0;
		
		private var recoilVec:Point;
		
		public function TowerGun(x:int, y:int, img:Image, lvl:LevelScreen ) 
		{
			super("T2", x, y, img, lvl, range, TowerGun.damage, 200);
			shootPoint  = new Point(Context.values.tileUnit / 2, 0);
			addEventListener(EnterFrameEvent.ENTER_FRAME, onFrame);
			shootPoint = new Point(Context.values.tileUnit / 2, Context.values.tileUnit * 0.078);
		}
		
		override public function clicked():void
		{
			super.clicked();
		}
		
		protected override function onShoot():void
		{
			Context.assets.manager.playSound("single-shot", 0, 0, new SoundTransform(0.05));
			recoilVec = new Point(target.x - x, target.y - y);
			recoilVec.normalize(1);
			TweenLite.to(this, 0.09, {x:this.x - recoilVec.x, y:this.y - recoilVec.y, ease:RoughEase.ease.config({strength:4, points:10, template:Linear.easeNone, randomize:false}), onComplete:reverseRecoil});
			
			shootPoint = shootPoints[lastSpUsed];
			lastSpUsed = (lastSpUsed + 1) % shootPoints.length;
			super.onShoot();
		}
		
		private function reverseRecoil():void
		{
			TweenLite.to(this, 0.09, {x:this.x + recoilVec.x, y:this.y + recoilVec.y, ease:RoughEase.ease.config({strength:4, points:10, template:Linear.easeNone, randomize:false})});
		}
	}

}