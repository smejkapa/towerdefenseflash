package td.mechanics 
{
	import com.adobe.tvsdk.mediacore.ItemLoaderListener;
	import flash.geom.Point;
	import starling.display.Sprite;
	import starling.display.Image;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import td.Context;
	import td.screens.LevelScreen;
	import td.utils.MathUtils;
	import td.utils.Utils;
	import starling.display.Quad;
	import flash.utils.getTimer;
	
	/**
	 * ...
	 * @author Aldarion
	 */
	public class Tower extends Sprite
	{
		private var code:String;
		private var image:Image;
		private var lvl:LevelScreen;
		private var range:int;
		private var damage:int;
		private var cooldown:int;
		
		protected var target:Enemy;
		private var forward:Point = new Point(0, -1);
		protected var shootPoint:Point;
		
		protected var lastShot:int;
		
		public function Tower(code:String, x:int, y:int, img:Image, lvl:LevelScreen, range:int, damage:int, cooldown:int) 
		{
			this.code = code;
			this.x = x + Context.values.tileUnit / 2;
			this.y = y + Context.values.tileUnit / 2;
			this.width = Context.values.tileUnit;
			this.height = Context.values.tileUnit;
			image = img;
			image.width = Context.values.tileUnit;
			image.height = Context.values.tileUnit;
			addChild(image);
			this.lvl = lvl;
			this.range = range;
			this.damage = damage;
			this.cooldown = cooldown;
			alignPivot();
			pivotY = Context.values.tileUnit * 0.68;
			
			addEventListener(EnterFrameEvent.ENTER_FRAME, onFrame);
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		
		private function addedToStage():void
		{
		}
		
		public function clicked():void
		{
		}
		
		private function setRotation(a:Point):void
		{
			var next:Point = a.subtract(new Point(x, y));
			next.normalize(1);
			this.rotation = Math.acos(MathUtils.dot(next, forward));
			if (next.x < 0)
			{
				this.rotation = -this.rotation;
			}
		}
		
		public function onFrame(event:EnterFrameEvent):void
		{
			if (target != null && (MathUtils.distance(target.x, target.y, x, y) > range || target.isDead() || target.isEscaped()))
			{
				target = null;
			}
			
			if (target == null)
			{
				for each (var enemy:Enemy in lvl.enemies)
				{
					if (MathUtils.distance(enemy.x, enemy.y, x, y) <= range)
					{
						target = enemy;
						break;
					}
				}
			}
			
			if (target != null)
			{
				setRotation(new Point(target.x, target.y));
				var time:int = getTimer();
				if (time - lastShot >= cooldown)
				{
					lastShot = time;
					onShoot();
				}
			}
		}
		
		protected function onShoot():void
		{
			var globSp:Point = localToGlobal(shootPoint);
			globSp = lvl.globalToLocal(globSp);
			var projectile:Projectile = ProjectileFactory.makeProjectile(code, globSp.x, globSp.y, lvl, target, damage);
			lvl.addChildAt(projectile, lvl.getChildIndex(this));
		}
	}

}