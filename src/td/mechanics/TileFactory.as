package td.mechanics 
{
	import td.Context;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author Aldarion
	 */
	public class TileFactory 
	{
		public static function makeTile(code:String, x:int, y:int):Tile
		{
			switch (code) 
			{
			case "#":
				return new Tile(code, x * Context.values.tileUnit, y * Context.values.tileUnit, Context.newImage("towerDefense_tile124.png"), null);
			case ".":
				return new Tile(code, x * Context.values.tileUnit, y * Context.values.tileUnit, Context.newImage("towerDefense_tile093.png"), null, false, true);
			case "S":
				return new Tile(code, x * Context.values.tileUnit, y * Context.values.tileUnit, Context.newImage("start"), null, false, true);
			case "E":
				return new Tile(code, x * Context.values.tileUnit, y * Context.values.tileUnit, Context.newImage("end"), null, false, true);
			case "B":
				return new Tile(code, x * Context.values.tileUnit, y * Context.values.tileUnit, Context.newImage("towerDefense_tile039.png"), null, true);
			default:
				throw "Invalid tile code";
			}
		}
	}

}