package td.mechanics 
{
	import td.Context;
	import td.screens.LevelScreen;
	/**
	 * ...
	 * @author Aldarion
	 */
	public class TowerFactory 
	{
		
		public static function makeTower(code:String, x:int, y:int, lvl:LevelScreen):Tower
		{
			switch (code) 
			{
				case "T1":
					return new TowerRocket(x * Context.values.tileUnit, y * Context.values.tileUnit, Context.newImage("towerDefense_tile249.png"), lvl);
				case "T2":
					return new TowerGun(x * Context.values.tileUnit, y * Context.values.tileUnit, Context.newImage("towerDefense_tile250.png"), lvl);
				default:
					throw "Unknown tower code";
			}
		}
		
		public static function getCost(code:String):int
		{
			switch (code) 
			{
				case "T1":
					return 14;
				case "T2":
					return 20;
				default:
					throw "Unknown tower code";
			}
		}
		
		public static function getInfo(code:String):String
		{
			switch (code) 
			{
				case "T1":
					return "Rocket tower cost: " + getCost(code);
				case "T2":
					return "Gun turret cost: " + getCost(code);
				default:
					throw "Unknown tower code";
			}
		}
	}

}