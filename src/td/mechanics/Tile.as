package td.mechanics 
{
	import flash.events.Event;
	import td.screens.LevelScreen;
	
	import td.particles.ParticlesExample;
	import td.ui.TextButton;
	
	import td.Context;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.TouchEvent;
	
	import com.greensock.TweenLite;
	import com.greensock.easing.Elastic;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author Aldarion
	 */
	public class Tile extends Sprite
	{
		private var backgroundImg: Image;
		public var code:String;
		
		private var onClick: Function;
		private var tower:Tower;
		private var isBuildable:Boolean;
		public var isWalkable:Boolean;
		
		public function Tile(code:String, x:int, y:int, img:Image, onClick:Function, isBuildable:Boolean = false, isWalkable = false)
		{
			this.x = x;
			this.y = y;
			this.width = Context.values.tileUnit;
			this.height = Context.values.tileUnit;
			backgroundImg = img;
			backgroundImg.width = Context.values.tileUnit;
			backgroundImg.height = Context.values.tileUnit;
			addChild(backgroundImg);
			this.onClick = onClick;
			this.code = code;
			this.isBuildable = isBuildable;
			this.isWalkable = isWalkable;
		}
		
		public function clicked(lvl:LevelScreen):void
		{
			if (this.onClick != null)
			{
				this.onClick();
			}
			if (tower != null)
			{
				tower.clicked();
			}
			if (isBuildable && tower == null && lvl.draggedId != null && lvl.draggedId != "")
			{
				var goldNeeded:int = TowerFactory.getCost(lvl.draggedId);
				if (lvl.getGold() < goldNeeded)
				{
					lvl.setInfo("Not enough gold, needed: " + goldNeeded);
				}
				else
				{
					lvl.spendGold(goldNeeded);
					tower = TowerFactory.makeTower(lvl.draggedId, this.x / Context.values.tileUnit, this.y / Context.values.tileUnit, lvl);
					parent.addChild(tower);
					var underTower:Image = Context.newImage("underTower");
					underTower.width = width;
					underTower.height = height;
					addChild(underTower);
					Context.assets.manager.playSound("building-placing", 200);
				}
			}
		}
	}

}