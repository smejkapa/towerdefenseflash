package td.mechanics 
{
	import flash.geom.Point;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import td.screens.LevelScreen;
	import td.utils.MathUtils;
	import td.Context;
	import com.greensock.TweenLite;
	import com.greensock.easing.Expo;
	import com.greensock.easing.Quad;
	/**
	 * ...
	 * @author Aldarion
	 */
	public class Projectile extends Sprite
	{
		private var visual:Sprite;
		private var explosionVisual:Sprite;
		private var target:Enemy;
		private var speed:int;
		private var lastPosition:Point;
		private var damage:int;
		private var lvl:LevelScreen;
		private static var forward:Point = new Point(0, -1);
		private var explosionFadeTime:Number;

		public function Projectile(x:int, y:int, visual:Sprite, target:Enemy, speed:Number, damage:int, lvl:LevelScreen, expl:Sprite, explFade:Number) 
		{
			explosionVisual = expl;
			explosionFadeTime = explFade;
			this.visual = visual;
			alignPivot();
			
			this.x = x;
			this.y = y;
			width = visual.width;
			height = visual.height;
			addChild(visual);
			
			this.damage = damage;
			this.speed = speed * Context.values.tileUnit;
			lastPosition = new Point(target.x, target.y);
			this.target = target;
			this.lvl = lvl;
			lvl.addChild(this);
			
			addEventListener(EnterFrameEvent.ENTER_FRAME, onFrame);
		}
		
		private function onFrame(event:EnterFrameEvent):void
		{
			if (target != null && (target.isEscaped() || target.isDead()))
			{
				target = null;
			}
			if (target != null)
			{
				lastPosition.x = target.x;
				lastPosition.y = target.y;
			}
			
			var dist:Number = MathUtils.distance(x, y, lastPosition.x, lastPosition.y);
			if (dist < 5)
			{
				if (target != null)
				{
					target.takeDmg(damage);
				}
				removeEventListeners();
				removeChild(visual);
				addChild(explosionVisual);
				TweenLite.to(this, explosionFadeTime, {x:this.x, y:this.y, alpha:0, ease:Quad.easeIn, onComplete:afterExplosion});
				return;
			}
			else
			{
				var direction:Point = lastPosition.subtract(new Point(x, y));
				direction.normalize(1);
				this.x += direction.x * event.passedTime * speed;
				this.y += direction.y * event.passedTime * speed;
				setRotation(lastPosition);
			}
		}
		
		private function afterExplosion():void
		{
			lvl.removeChild(this);
		}
		
		private function setRotation(a:Point):void
		{
			var next:Point = a.subtract(new Point(x, y));
			next.normalize(1);
			this.rotation = Math.acos(MathUtils.dot(next, forward));
			if (next.x < 0)
			{
				this.rotation = -this.rotation;
			}
		}
	}

}