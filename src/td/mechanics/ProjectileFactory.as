package td.mechanics 
{
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import td.screens.LevelScreen;
	import td.Context;
	/**
	 * ...
	 * @author Aldarion
	 */
	public class ProjectileFactory 
	{
		
		public static function makeProjectile(code:String, x:int, y:int, lvl:LevelScreen, enemy:Enemy, damage:int):Projectile
		{
			var visual:Sprite = new Sprite();
			var impact:Sprite = new Sprite();
			switch (code) 
			{
				case "T1": // Rocket
				{
					var rocket:Image = Context.newImage("rocket");
					rocket.alignPivot();
					visual.addChild(rocket);
					visual.alignPivot();
					visual.width = Context.values.tileUnit;
					visual.height = visual.width;
					var tail:Image = Context.newImage("tail");					
					tail.alignPivot();
					tail.rotation = Math.PI;
					tail.pivotY = Context.values.tileUnit * 0.679;
					visual.addChild(tail);
					tail.y = (Context.values.tileUnit * 0.5);
					tail.width = tail.width / 2;
					tail.height = tail.width;
					
					var explImg:Image = Context.newImage("explosion");
					impact.addChild(explImg);
					impact.alignPivot();
					impact.width = Context.values.tileUnit;
					impact.height = impact.width;
					
					return new Projectile(x, y, visual, enemy, 3, damage, lvl, impact, 5);
				}
				case "T2": // Gun
				{
					var size:int = Math.floor(Math.max(4, Context.values.tileUnit / 12));
					var q:Quad = new Quad(size, size);
					q.color = 256 * 256 * 256;
					visual.addChild(q);
					var bloodQuad:Quad = new Quad(size * 0.8, size * 1.2);
					bloodQuad.color = 0x8A0707;
					impact.addChild(bloodQuad);
					
					return new Projectile(x, y, visual, enemy, 5, damage, lvl, impact, 1);
				}
				default:
					throw "Unknown tower code";
			}
		}
		
	}

}