package td.mechanics 
{
	import td.screens.LevelScreen;
	import td.Context;
	/**
	 * ...
	 * @author Aldarion
	 */
	public class EnemyFactory 
	{
		
		public static function makeEnemy(code:int, path:Array, lvl:LevelScreen):Enemy
		{
			switch (code) 
			{
				case 0:
					return new Enemy(2.25, 3, 10, Context.newImage("towerDefense_tile245.png"), lvl, path);
				case 1:
					return new Enemy(1.5, 1, 8, Context.newImage("towerDefense_tile247.png"), lvl, path);
				case 2:
					return new Enemy(1, 5, 30, Context.newImage("towerDefense_tile246.png"), lvl, path);
				default:
					throw "Unknown enemy code";
			}
		}
		
	}

}