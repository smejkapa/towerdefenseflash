package td.screens 
{
	import flash.events.Event;
	
	import td.particles.ParticlesExample;
	import td.ui.TextButton;
	
	import td.Context;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.TouchEvent;
	
	import com.greensock.TweenLite;
	import com.greensock.easing.Elastic;
	
	import starling.core.Starling;
	import td.mechanics.Maps;
	import flash.media.SoundChannel;
	//import flash.desktop.NativeApplication;	
	import flash.media.SoundTransform;
	
	public class MenuScreen extends Sprite
	{
		private var background: Sprite;
		
		private var backgroundImg: Image;
		
		private var playButtons:Array;
		private var quitButton:TextButton;
		
		public function MenuScreen() 
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage); 
		}
		
		private function onAddedToStage(e: * = null) : void 
		{
			Context.assets.manager.playSound("world-war-theme", 0, 999, new SoundTransform(0.3));
			
			Starling.current.antiAliasing = 4;
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage); 
			
			background = new Sprite();
			background.x = Context.assets.stageWidth / 2;
			background.y = Context.assets.stageHeight / 2;
			background.width = Starling.current.viewPort.width;
			background.height = Starling.current.viewPort.height;
			addChild(background);
			
			backgroundImg = Context.newImage("background.png");
			background.addChild(backgroundImg);
			
			background.alignPivot();
			
			background.scale = 0;
			Context.v.tweens.menu.duration = 1;
			TweenLite.to(background, Context.v.tweens.menu.duration, {ease: Elastic.easeOut.config(0.1, 0.5), scale: 1});
			
			var mapCount:int = Maps.getMapCount();
			var nTh:int = Starling.current.viewPort.width / mapCount;
			playButtons = new Array();
			for (var i:int = 0; i < mapCount; i++)
			{
				var locI:int = i;
				var playB:TextButton = new TextButton("Map " + (i + 1), 16, makePlayClosure(locI));
				addChild(playB);
				playB.alignPivot();
				playB.x = i * nTh + nTh / 2;
				playB.y = y + height / 2;
			}
		}
		
		private function makePlayClosure(lvl:int):Function
		{
			return function(event: TouchEvent = null):void { onPlay(lvl); };
		}
		
		private function onPlay(lvl:int) : void
		{
			trace("Play level: " + lvl);
			Context.showScreen(new LevelScreen(lvl));
		}
		
		private function quitGame():void
		{
			// Does not work
			// NativeApplication.nativeApplication.exit();
		}
		
	}

}