package td.screens 
{
	import flash.events.Event;
	import starling.events.Touch;
	import starling.text.TextFormat;
	import td.mechanics.Enemy;
	import td.mechanics.Tile;
	import td.mechanics.TileFactory;
	
	import td.particles.ParticlesExample;
	import td.ui.TextButton;
	
	import td.Context;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.TouchEvent;
	import starling.core.Starling;
	
	import com.greensock.TweenLite;
	import com.greensock.easing.Elastic;
	import flash.geom.Point;
	import starling.events.TouchPhase;
	import td.mechanics.MenuFactory;
	import td.mechanics.EnemyFactory;
	import starling.events.EnterFrameEvent;
	import starling.text.TextField;
	import starling.utils.Color;
	import starling.utils.Align;
	import flash.utils.getTimer;
	import td.mechanics.TowerFactory;
	import td.mechanics.Maps;
	
	/**
	 * ...
	 * @author Aldarion
	 */
	public class LevelScreen extends Sprite
	{
		private var mapString:Array;
		private var map:Array;
		public var draggedId:String;
		private var draggedImg:Sprite;
		
		private var menuBar:Array;
		private var levelIndex:int;
		
		private var gold:int = 40;
		private var goldTotal:int = gold;
		public function getGold():int { return gold; }
		private var escaped:int = 0;
		private var escapedMax:int = 10;
		
		public var enemies:Array;
		
		private var enemyPath:Array;
		private var start:Point;
		private var end:Point;
		
		private var goldText:TextField;
		private var escapedText:TextField;
		private var infoText:TextField;
		private var textFormat:TextFormat;
		private var timeText:TextField;
		
		private var elapsed:Number = 0;
		private var placementTime:Number = 5;
		
		private var spawnCooldown:int = 4000;
		private var minSpawnCooldown:int = 500;
		private var nextSpawn:int = 0;
		
		private var menuButton: TextButton;
		
		public function LevelScreen(level: int) 
		{
			this.levelIndex = level;
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			addEventListener(TouchEvent.TOUCH, onMouseClick);
			addEventListener(EnterFrameEvent.ENTER_FRAME, onFrame);
		}
		
		private function onAddedToStage(e: * = null) : void
		{
			mapString = Maps.getMap(levelIndex);
			
			map = new Array();
			for (var y:int = 0; y < mapString.length; y++) 
			{
				map[y] = new Array();
				var line:Array = mapString[y];
				for (var x:int = 0; x < line.length; x++) 
				{				
					var tile:Tile = TileFactory.makeTile(line[x], x, y);
					map[y][x] = tile;
					addChild(map[y][x]);
					if (line[x] == 'S')
					{
						start = new Point(x, y);
					}
					else if (line[x] == 'E')
					{
						end = new Point(x, y);
					}
				}
			}
			enemyPath = findEnemyPath();
			
			var menuString:Array = ["T1", "T2"];
			
			menuBar = new Array();
			for (x = 0; x < menuString.length; x++)
			{
				menuBar.push(MenuFactory.makeButton(menuString[x], x, mapString.length));
				addChild(menuBar[x]);
			}
			
			enemies = new Array();
			
			textFormat = new TextFormat();
			textFormat.color = Color.WHITE;
			textFormat.size = 14;
			textFormat.horizontalAlign = Align.LEFT;
			
			infoText = new TextField(Context.values.tileUnit * (map[0].length - menuBar.length), Context.values.tileUnit, "Info bar", textFormat);
			addChild(infoText);
			infoText.x = menuBar[menuBar.length - 1].x + menuBar[menuBar.length - 1].width;
			infoText.y = menuBar[menuBar.length - 1].y;
			
			goldText = new TextField(Context.values.tileUnit * 2, Context.values.tileUnit, "", textFormat);
			updateGoldText();
			addChild(goldText);
			goldText.x = 0;
			goldText.y = (map.length + 1) * Context.values.tileUnit;
			
			escapedText = new TextField(Context.values.tileUnit * 3, Context.values.tileUnit, "", textFormat);
			updateEscapedText();
			addChild(escapedText);
			escapedText.x = goldText.x + goldText.width;
			escapedText.y = goldText.y;
			
			timeText = new TextField(Context.values.tileUnit * 2, Context.values.tileUnit, formatTime(elapsed - placementTime), textFormat);
			addChild(timeText);
			timeText.x = escapedText.x + escapedText.width;
			timeText.y = escapedText.y;
			
			menuButton = new TextButton("Menu", 16, goToMenu);
			addChild(menuButton);
			menuButton.x = timeText.x + timeText.width;
			menuButton.y = timeText.y;
		}
	
		private function goToMenu(event: TouchEvent = null):void
		{
			Context.showScreen(new MenuScreen());
		}
		
		public static function formatTime(time:int):String
		{
			var minus:String = time < 0 ? "-" : "";
			time = Math.abs(Math.floor(time));
			var sec:int = int(time % 60);
			var secStr:String = sec < 10 ? "0" + sec : "" + sec;
			return minus + int(time / 60) + ":" + secStr;
		}
		
		public function setInfo(msg:String):void
		{
			infoText.text = msg;
		}
		
		private function findEnemyPath():Array
		{
			var directions:Array = [new Point(-1, 0), new Point(1, 0), new Point(0, -1), new Point(0, 1)];
			var next:Point = start;
			var prev:Point = start;
			var path:Array = new Array();
			
			while (!next.equals(end))
			{
				path.push(new Point(next.x * Context.values.tileUnit + Context.values.tileUnit / 2, next.y * Context.values.tileUnit + Context.values.tileUnit / 2));
				var foundNext:Boolean = false;
				// Find next
				for (var i:int = 0; i < directions.length;i++)
				{
					var d:Point = directions[i];
					var np:Point = next.add(d);
					if (isValidCoord(np) && !np.equals(prev) && map[np.y][np.x].isWalkable)
					{
						prev = next;
						next = np;
						foundNext = true;
						break;
					}
				}
				if (!foundNext)
				{
					throw "Invalid path detected";
				}
			}
			path.push(new Point(next.x * Context.values.tileUnit + Context.values.tileUnit / 2, next.y * Context.values.tileUnit + Context.values.tileUnit / 2));
			return path;
		}
		
		private function isValidCoord(p:Point):Boolean
		{
			return p.x >= 0 &&
				p.y >= 0 &&
				p.x < map[0].length &&
				p.y < map.length;
		}
		
		private function onMouseClick(e: TouchEvent) : void
		{
			var touch:Touch = e.getTouch(this.stage);
			if (touch)
			{
				var loc:Point = touch.getLocation(stage);
				var x:int = loc.x / Context.values.tileUnit;
				var y:int = loc.y / Context.values.tileUnit;
				if(touch.phase == TouchPhase.BEGAN) // Mouse down
                {
					if (y == map.length)
					{
						if (x < menuBar.length)
						{
							menuBar[x].clicked(this);
							if (x < 2)
							{
								draggedImg = MenuFactory.makeTowerImage(menuBar[x].code);
								draggedImg.alignPivot();
								addChild(draggedImg);
								draggedImg.x = loc.x;
								draggedImg.y = loc.y;
								draggedId = menuBar[x].code;
								setInfo(TowerFactory.getInfo(draggedId));
							}
							else if (x == 2)
							{
								
							}
						}
					}
                }
				else if(touch.phase == TouchPhase.ENDED) // Mouse up
                {
					if (y < map.length)
					{
						map[y][x].clicked(this);
					}
					removeChild(draggedImg);
					draggedId = "";
					draggedImg = null;
                }
				else if(touch.phase == TouchPhase.MOVED) // Drag
                {
					if (draggedImg != null)
					{
						draggedImg.x = loc.x;
						draggedImg.y = loc.y;
					}
                }
			}
		}
	
		public function enemyEscaped(e:Enemy):void
		{
			escaped += 1;
			updateEscapedText();
			if (escaped >= escapedMax)
			{
				Context.showScreen(new GameOverScreen(goldTotal, elapsed - placementTime));
			}
		}
		
		public function coinPickedUp(value:int):void
		{
			gold += value;
			goldTotal += value;
			updateGoldText();
		}
		
		private function onFrame(event:EnterFrameEvent):void
		{
			elapsed += event.passedTime;
			timeText.text = formatTime(elapsed - placementTime);
			
			for (var i:int = enemies.length - 1; i >= 0; i--)
			{
				if (enemies[i].isEscaped() || enemies[i].isDead())
				{
					removeChild(enemies[i]);
					enemies.removeAt(i);
				}
			}
			
			var time:int = getTimer();
			if (elapsed > placementTime && time > nextSpawn)
			{
				spawnCooldown = Math.max(spawnCooldown - 10, minSpawnCooldown);
				nextSpawn = time + spawnCooldown + ((spawnCooldown / 2) * Math.random() - spawnCooldown / 2);
				var type:int = (Math.floor(Math.random() * 4) + 1) % 3;
				var enemy:Enemy = EnemyFactory.makeEnemy(type, enemyPath, this);
				enemies.push(enemy);
				addChild(enemy);
			}
		}
		
		public function spendGold(amount:int):void
		{
			gold -= amount;
			updateGoldText();
		}
		
		private function updateGoldText():void
		{
			goldText.text = "Gold: " + gold;
		}
		
		private function updateEscapedText():void
		{
			escapedText.text = "Escaped: " + escaped + "/" + escapedMax
		}
	}
}