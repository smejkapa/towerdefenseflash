package td.screens 
{
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.text.TextFormat;
	import starling.core.Starling;
	import starling.utils.Color;
	import td.ui.TextButton;
	import td.Context;
	/**
	 * ...
	 * @author Aldarion
	 */
	public class GameOverScreen extends Sprite
	{
		private var textFormat:TextFormat;
		private var message:TextField;
		private var menuButton: TextButton;
		
		public function GameOverScreen(gold:int, time:int) 
		{
			textFormat = new TextFormat();
			textFormat.size = 16;
			textFormat.color = Color.WHITE;
			
			var msg:String = "Game over\nGold collected: " + gold + "\nTime elapsed: " + LevelScreen.formatTime(time);
			message = new TextField(Starling.current.viewPort.width, Starling.current.viewPort.height / 2, msg, textFormat);
			addChild(message);
			
			menuButton = new TextButton("Go to menu", 16, goToMenu);
			addChild(menuButton);
			menuButton.alignPivot();
			menuButton.x = Starling.current.viewPort.width / 2;
			menuButton.y = Starling.current.viewPort.height / 2;
		}
		
		private function goToMenu(e:* = null):void
		{
			Context.showScreen(new MenuScreen());
		}
		
	}

}