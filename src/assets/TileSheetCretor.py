import xml.etree.ElementTree as ET

NAME_TMPL = 'towerDefense_tilesheet_{}.xml'
TEMPLATE = 'towerDefense_tilesheet_template.xml'
SIZE = 96

print('Modifying XML from template')

tree = ET.parse(TEMPLATE)
root = tree.getroot()
root.set('imagePath', NAME_TMPL.format(SIZE))

for node in root.iter('SubTexture'):
    x = int(node.get('x')) * SIZE
    y = int(node.get('y')) * SIZE
    node.set('x', str(x))
    node.set('y', str(y))
    node.set('width', str(SIZE))
    node.set('height', str(SIZE))

tree.write(NAME_TMPL.format(SIZE))

print('Done')
